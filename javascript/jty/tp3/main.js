$.ajax('http://localhost:8080/parties').done((data) => {
  data.forEach(party => {
    $('#parties').append(`
      <div class="card">
        <div class="card-body">
          <h3>${party.name}</h3>
          <span>${party.date}</span>
          <span>${party.location.name}</span>
          <button type="button" class="btn btn-info detail-btn" data-party-id="${party.id}">Details</button>
        </div>
      </div>
    `);
  });
  setClickListeners();
});

function setClickListeners() {
  $('.detail-btn').click((e) => {
    console.log($(e.target).attr('data-party-id'));
  });
}
